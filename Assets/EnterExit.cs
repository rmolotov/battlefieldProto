﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Utility;

public class EnterExit : MonoBehaviour {

    public Camera PlayerCamera;
    public Transform Player;
    public Transform Vehicle;
    public Camera VehicleCamera;
    public KeyCode Enter;
    public Transform ExitPosition;

    public Component[] comps;

    private bool isIn = false;
    private bool inTrig = false;

	// Use this for initialization
	void Awake () {
        Vehicle.GetComponent<TankTrackController>().enabled = false;
        Vehicle.GetComponent<TurretRotate>().enabled = false;
        Vehicle.GetComponent<UpAbdDown>().enabled = false;
        Vehicle.Find("Head").GetComponent<UpAbdDown>().enabled = false;
        Vehicle.Find("Head").Find("Antenna").GetComponent<Animator>().enabled = false;
        Vehicle.Find("Head").Find("Antenna").GetComponent<AntennaScript>().enabled = false;
        VehicleCamera.GetComponent<CameraScript>().enabled = false;
        VehicleCamera.GetComponent<ThirdPersonCamera>().enabled = false;
        VehicleCamera.GetComponent<SimpleMouseRotator>().enabled = false;
        Vehicle.GetComponent<AudioSource>().enabled = false;
        Vehicle.GetComponent<AudioListener>().enabled = false;
        
        //foreach(Component c in comps)
        //{
        //    var a = c.GetType();
        //    var comp = Vehicle.GetComponent<a.Name>();
        //    comp.enabled = false;
        //}

    }

    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            inTrig = true;
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.tag == "Player")
        {
            inTrig = false;
        }
    }

    // Update is called once per frame
    void Update () {
        if (Input.GetKeyDown(Enter))
        {
            //вход
            if(inTrig && !isIn)
            {
                Player.gameObject.SetActive(false);
                Player.transform.parent = Vehicle;
                Vehicle.GetComponent<TankTrackController>().enabled = true;
                Vehicle.GetComponent<TurretRotate>().enabled = true;
                Vehicle.GetComponent<UpAbdDown>().enabled = true;
                Vehicle.Find("Head").GetComponent<UpAbdDown>().enabled = true;
                VehicleCamera.GetComponent<CameraScript>().enabled = true;
                Vehicle.Find("Head").Find("Antenna").GetComponent<Animator>().enabled = true;
                Vehicle.Find("Head").Find("Antenna").GetComponent<AntennaScript>().enabled = true;
                Vehicle.GetComponent<AudioSource>().enabled = true;
                Vehicle.GetComponent<AudioSource>().Play();
                Vehicle.GetComponent<AudioListener>().enabled = true;
                Player.localPosition = ExitPosition.localPosition;
                PlayerCamera.enabled = false;
                VehicleCamera.enabled = true;
                isIn = true;
            }
            //выход
            else if (isIn)
            {
                Player.gameObject.SetActive(true);
                Player.transform.parent = null;
                Vehicle.GetComponent<TankTrackController>().enabled = false;
                Vehicle.GetComponent<TurretRotate>().enabled = false;
                Vehicle.GetComponent<UpAbdDown>().enabled = false;
                Vehicle.Find("Head").GetComponent<UpAbdDown>().enabled = false;
                Vehicle.Find("Head").Find("Antenna").GetComponent<Animator>().enabled = false;
                Vehicle.Find("Head").Find("Antenna").GetComponent<AntennaScript>().enabled = false;
                VehicleCamera.GetComponent<CameraScript>().enabled = false;
                VehicleCamera.GetComponent<ThirdPersonCamera>().enabled = false;
                VehicleCamera.GetComponent<SimpleMouseRotator>().enabled = false;
                Vehicle.GetComponent<AudioSource>().enabled = false;
                Vehicle.GetComponent<AudioListener>().enabled = false;
                PlayerCamera.enabled = true;
                VehicleCamera.enabled = false;
                isIn = false;
            }
        }
    }
}
