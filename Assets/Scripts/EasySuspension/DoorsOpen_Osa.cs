﻿using UnityEngine;
using System.Collections;

public class DoorsOpen_Osa : MonoBehaviour {

    [Range(50, 500)]
    public int speed;

    public Transform leflDoor;
    public Transform rightDoor;
    public float closedYcord;
    public float openedYcord = 75;

    bool isMoving = false;
    bool isNotOpen = true;

	// Use this for initialization
	void Start () {
        closedYcord = leflDoor.localRotation.eulerAngles.y;
	}

	// Update is called once per frame
	void Update () {
        if (Input.GetKeyUp(KeyCode.Return))
        {
            isMoving = true;
        }
        if (isMoving)
        {
            if (isNotOpen) //открываем
            {
                leflDoor.Rotate(Vector3.up, speed * Time.deltaTime, Space.Self);
                //leflDoor.RotateAroundLocal(Vector3.up, speed * Time.deltaTime);
                if (leflDoor.localRotation.eulerAngles.y >= openedYcord)
                {
                    isMoving = false;
                    isNotOpen = false;
                    leflDoor.Rotate(Vector3.up, (openedYcord - leflDoor.localRotation.eulerAngles.y));
                }
            }
            else //закрываем
            {
                leflDoor.Rotate(-Vector3.up, speed * Time.deltaTime, Space.Self);
                //leflDoor.RotateAroundLocal(-Vector3.up, speed * Time.deltaTime);
                if (leflDoor.localRotation.eulerAngles.y <= speed / 50)
                {
                    isMoving = false;
                    isNotOpen = true;
                    leflDoor.Rotate(Vector3.up, (closedYcord - leflDoor.localRotation.eulerAngles.y));
                }
            }
        }
    }
}
