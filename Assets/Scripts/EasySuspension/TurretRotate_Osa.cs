﻿using UnityEngine;
using System.Collections;

public class TurretRotate_Osa : MonoBehaviour {

    public Transform turret;
    public int speed;
	
	public float closedYcord;
    public float openedYcord = 5;

    bool isMoving = false;
    bool isNotOpen = true;

	// Use this for initialization
	void Start () {
	
	//closedYcord = turret.localRotation.eulerAngles.y;
	
	}
	
	// Update is called once per frame
	void Update () {
	    if(Input.GetKey(KeyCode.LeftArrow))
        {
            turret.Rotate(-Vector3.up, 10* speed * Time.deltaTime, Space.Self);
        } else
        if (Input.GetKey(KeyCode.RightArrow))
        {
            turret.Rotate(Vector3.up, 10* speed * Time.deltaTime, Space.Self);
        }
    }
}
