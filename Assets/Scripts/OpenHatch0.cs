﻿using UnityEngine;
using System.Collections;

public class OpenHatch0 : MonoBehaviour {

    public KeyCode open;

    private Animator anim;

    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyUp(open)){
            anim.SetBool("Open", !anim.GetBool("Open"));
        }
	}
}
