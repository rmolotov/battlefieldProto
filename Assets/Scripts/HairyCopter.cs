﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
 
public class HairyCopter : MonoBehaviour {
    private KeyCode UpKey = KeyCode.RightShift;
    private KeyCode DownKey = KeyCode.RightControl;
    private KeyCode FrontSpin = KeyCode.UpArrow;
    private KeyCode BackWardSpin = KeyCode.DownArrow;
    private KeyCode LeftTurn = KeyCode.Keypad4;
    private KeyCode RightTurn = KeyCode.Keypad6;
    private KeyCode LeftSpin = KeyCode.LeftArrow;
    private KeyCode RightSpin = KeyCode.RightArrow;
    private KeyCode EngineOnOffKey = KeyCode.E;

    public int maxAltitude;
 
    //Helicopter Big Propeller. Use Y Rotate Value.
    public GameObject[] MainMotor; 
    //Helicopter Small Propeller. User X RotateValue.    
    public GameObject[] SubMotor; 
    //Helicopter Engine Sound.
    public AudioSource _AudioSource; 
 
    public float MainMotorRotateVelocity;
    public float SubMotorRotateVelocity;
 
    //Rigidbody variable for controlling drag, high drag needed to retard motion, low drag needed to prevent 'floating' when engine is off.
    public Rigidbody rb;        
     
    /*  Not currently used
    * //Text for de-bugging.
    * public GUIElement pitchInputValue;
    * public GUIElement VertV;
    * public GUIElement PitchAngle;
    */
     
    float VerticalVelocity = 0.0f; 
    float MainMotorRotation = 0.0f;
    float SubMotorRotation = 0.0f;    
    public float ClimbRate;
    float AltitudeInput;
 
    //Pitch Values
    float Pitch;
    float PitchInput;
    float forwardSpeed;
 
    // Yaw Variables
    float Yaw = 150;
    float LeftRightTurn;
 
    // Roll Variables
    float Roll;
    float LeftRightSpin;
    float sideSpeed;
 
    bool EngineOn = false;
    public float EngineValue = 0.0f;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        //rb.centerOfMass += new Vector3(0, 14, 100);
    }
 
 //Runs every physics step, always a fixed interval, best used for rigidbody physics updates.
    void FixedUpdate()
    {
        //MotorVelocityContorl(); // Check Helicoptor AltitudeInput State

        if (EngineOn)
        {
            //Applies force on the current vertical axis of the Helicopter
            GetComponent<Rigidbody>().AddRelativeForce(Vector3.up * ClimbRate * VerticalVelocity);
            GetComponent<Rigidbody>().AddRelativeForce(Vector3.right * sideSpeed);
            // additional force to counter altitude loss – note can result in climbing if not at full roll or when used with pitch
            GetComponent<Rigidbody>().AddForce(Vector3.up * Mathf.Abs(sideSpeed * 0.33f));
            GetComponent<Rigidbody>().AddRelativeForce(Vector3.forward * forwardSpeed);
            GetComponent<Rigidbody>().AddForce(Vector3.up * Mathf.Abs(forwardSpeed * 0.33f));
            // High drag to damp motion
            rb.drag = 2;

            //Sets Max / Min Altitude
            if (transform.position.y > maxAltitude)
                GetComponent<Rigidbody>().AddRelativeForce(-Vector3.up * ClimbRate * VerticalVelocity);

            AltitudeInput = KeyValue(DownKey, UpKey);
            //AltitudeInput = Input.GetAxis("Horizontal");

            PitchInput = KeyValue(BackWardSpin, FrontSpin);
            LeftRightTurn = KeyValue(LeftTurn, RightTurn);
            LeftRightSpin = KeyValue(LeftSpin, RightSpin);

            //Pitch Value
            if (PitchInput != 0)
            {
                Pitch += PitchInput * Time.fixedDeltaTime * 60f;
                //Limits the Pitch to a value of 20. 
                Pitch = Mathf.Clamp(Pitch, -20f, 20f);
                //additional forwards force to improve lateral movement, adjusting the integer increases the speed.
                forwardSpeed = PitchInput * 1000; //Constant initially 50 adjusted to 7 after rescaling SE.
            }
            else
            {
                Pitch = Mathf.Lerp(Pitch, 0, Time.fixedDeltaTime * 3.0f);
                // reset forward speed value
                forwardSpeed = 0.0f;
            }

            //Yaw Value
            //Yaw is unlimited allowing 360deg rotation
            Yaw += LeftRightTurn * Time.fixedDeltaTime * 60f;

                //Roll Value
            if (LeftRightSpin != 0)
            {
                Roll += -LeftRightSpin * Time.fixedDeltaTime * 60f;
                //Limits the roll to a value of 1.2
                Roll = Mathf.Clamp(Roll, -20f, 20f);
                //additional sideways force to improve lateral movement 
                sideSpeed = LeftRightSpin * 500;
                //Constant initially 50 adjusted to 7 after rescaling SE.

            }
            else
            {
                //Smooth the Roll back to 0
                Roll = Mathf.Lerp(Roll, 0, Time.fixedDeltaTime * 3.0f);
                //Reset the sideSpeed value - note, this line may not be required if the sideSpeed were taken outside of the if statement
                sideSpeed = 0.0f;
            }

            //Transforms from current orientation to desired new rotation, indicated by Pitch Yaw and Roll Values with a 1.5 Second smoothing factor
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(Pitch, Yaw, Roll), Time.fixedDeltaTime * 1.5f);


            /* Not Used in main system
                    //Debugging outputs 
                    PitchAngle.GetComponent<GUIText>().text = "Pitch Angle = " + transform.rotation;
                    VertV.GetComponent<GUIText>().text = "Vertical Velocity = " + VerticalVelocity;
                    pitchInputValue.GetComponent<GUIText>().text = "Pitch Input =" + PitchInput;
                    */
        }
        else
        {
            rb.drag = 0;
        }
    }
     
 // Update fires every Frame, time step dependent upon framerate
    void Update ()
    {
        //Debug.Log("updated");
        SoundControl(); // Check Helicopter Engine Sound
        MotorRotateControl(); // Check Motor Rotate State
        MotorVelocityControl(); // Check Helicopter AltitudeInput State
        EngineControl(); // Check Engine Turn On/Off
    }
 
    //Key input script, originally used to smooth inputs, no longer strictly required, but retained to allow future adjustments, maybe required for gesture controls.
    float KeyValue(KeyCode A, KeyCode B)
    {
        float Value = 0.0f;
        if (Input.GetKey (A))
            Value = -1.0f;
        else if (Input.GetKey (B))
            Value = 1.0f;
        else
            Value = 0;//As soon as keys are released returns a 0 value
        return Value;
    }
 
    void MotorVelocityControl()
    {
        if (EngineOn)
        {
            //for maintain altitude.
            float Hovering = (Mathf.Abs(GetComponent<Rigidbody>().mass * Physics.gravity.y) / ClimbRate); 
            if (AltitudeInput != 0.0f)
                //if Input Up/Down Axes, increase VerticalVelocity for Increase altitude.
                VerticalVelocity += AltitudeInput; 
            else
            //if no input on Vertical Axes revert to Hovering            
                VerticalVelocity = Hovering; 
 
 
            /* Original code included a smoothing function to gradually shift to hovering, when combined with the physics inertial effects this resulted in over-smoothing.  
            *  To resolve this the code line below was replaced with 'Hovering;'
            */ 
            Mathf.Lerp(VerticalVelocity, Hovering, Time.deltaTime); 
            /* 
            * This resulted in improved vertical handling.
            */
        }
        CheckVerticalVelocity();
    }
    void CheckVerticalVelocity()
    {
        VerticalVelocity = Mathf.Clamp(VerticalVelocity, -2.0f, 2.0f);
    }

    void MotorRotateControl()
    {
        if (MainMotor.Length > 0)
        {
            for (int i = 0; i < MainMotor.Length; i++)
                MainMotor[i].transform.Rotate(0, MainMotorRotation, 0);
        }
 
        if (SubMotor.Length > 0)
        {
            for (int i = 0; i < SubMotor.Length; i++)
                SubMotor[i].transform.Rotate(SubMotorRotation, 0, 0);
        }
        MainMotorRotation = MainMotorRotateVelocity * EngineValue * Time.deltaTime;
        SubMotorRotation = SubMotorRotateVelocity * EngineValue * Time.deltaTime;
    }
    void EngineControl()
    {
        if (Input.GetKeyDown(EngineOnOffKey))
        {
            EngineOn = !EngineOn;
        }
 
        if (EngineOn)
        {
            if (EngineValue < 1)
                EngineValue += Time.deltaTime / 5;
            else
                EngineValue = 1;
        }
        else
        {
            if (EngineValue > 0)
                EngineValue -= Time.deltaTime / 10;
            else
                EngineValue = 0;
        }
    }
    void SoundControl()
    {
        if (EngineValue > 0)
        {
            _AudioSource.mute = false;
            _AudioSource.pitch = EngineValue;
        }
        else
            _AudioSource.mute = true;
        //Debug.Log(_AudioSource.name);
    }
}