﻿using UnityEngine;
using System.Collections;

public class PlatformRotateEzh : MonoBehaviour {

    float openedYcoord = 260;
    float closedYcoord;
    public Transform platform;
    public bool isMoving = false;
    public bool isOpen = false;
    public bool isMovingBack = false;
    bool isConnected = false;
    public float speed = 10;
	// Use this for initialization
	void Start () {
        
        
        closedYcoord = platform.localRotation.eulerAngles.y;
    }
	
	// Update is called once per frame
	void Update () {
        isConnected = GameObject.Find("Supports").GetComponent<SupportsDownEzh>().isFinished;
 //       Debug.Log(isConnected);

        if (Input.GetKeyDown("2") && isConnected && !isOpen && !isMovingBack && !isMoving && isConnected) {
            isMoving = true;
            
        }
        if (isMoving)
        {
    //        Debug.Log("Moving!!!");
            transform.Rotate(Vector3.back * Time.deltaTime * speed);
        }
        if  (platform.localRotation.eulerAngles.z <= openedYcoord && isMoving)
        {
     //       Debug.Log("ISOPENTRUE!!");
            isMoving = false;
            isOpen = true;
        }
        if (!GameObject.Find("Stair").GetComponent<StairDownEzh>().isMoving && !GameObject.Find("Stair").GetComponent<StairDownEzh>().isMovingBack && !GameObject.Find("Stair").GetComponent<StairDownEzh>().isOpen && Input.GetKeyDown("2") && !isMoving && !isMovingBack && isConnected)
        {
            isMovingBack = true;
            isOpen = false;
        }
        if (isMovingBack)
        {
            transform.Rotate(Vector3.forward * Time.deltaTime * speed);
     //       Debug.Log(platform.localRotation.eulerAngles.z);
            if (platform.localRotation.eulerAngles.z > 359 && platform.localRotation.eulerAngles.z < 360)
                isMovingBack = false;

        }
        
	}
}
