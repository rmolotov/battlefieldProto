﻿using UnityEngine;
using System.Collections;

public class UpAbdDown : MonoBehaviour {

    public Transform[] Elements;
    public int speed;
    public int maxUp;

    private int num = 0;

    public KeyCode Up;
    public KeyCode Down;
    public KeyCode Switch;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(Up))
        {
            if(Elements[num].localRotation.eulerAngles.x <= 10 || Elements[num].localRotation.eulerAngles.x >= 360 - maxUp)
                Elements[num].Rotate(new Vector3(-5, 0, 0), speed * Time.deltaTime);
        }
        else if (Input.GetKey(Down))
        {
            if (Elements[num].localRotation.x <= -0.01 || Elements[num].localRotation.x >= 0.01)
                Elements[num].Rotate(new Vector3(5, 0, 0), speed * Time.deltaTime);
            else
                Elements[num].Rotate(Vector3.right, -Elements[num].localEulerAngles.x);
                       
        } else
        //        Debug.Log(Elements[0].localRotation.x);
        if (Input.GetKeyDown(Switch))
        {
            num++;
            if (num >= Elements.Length)
                num = 0;
        }
    }
}
