﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Utility;

public class ChangeCamera : MonoBehaviour {

	// Use this for initialization
    public Camera mycamera;
    public Transform[] viewPoints;
    public int currView = 0;
	void Start () {
        mycamera.transform.parent = viewPoints[currView];
        mycamera.transform.localPosition = Vector3.zero;
        mycamera.transform.localEulerAngles = Vector3.zero;
	}
	
    void LateUpdate()
    {
        if (Input.GetKeyUp(KeyCode.C))
        {
            Debug.Log(currView);
            if (currView == viewPoints.Length-1) currView = 0;
            else currView++;
            Debug.Log(currView);

            mycamera.transform.parent = viewPoints[currView];
            mycamera.transform.localPosition = Vector3.zero;
            mycamera.transform.localEulerAngles = Vector3.zero;

            if (currView > 0)
            {
                mycamera.GetComponent<ThirdPersonCamera>().enabled = false;
                mycamera.GetComponent<SimpleMouseRotator>().enabled = true;
            }
            else
            {
                mycamera.GetComponent<ThirdPersonCamera>().enabled = true;
                mycamera.GetComponent<SimpleMouseRotator>().enabled = false;
            }
        }
    }
}
