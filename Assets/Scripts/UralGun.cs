﻿using UnityEngine;
using System.Collections;


public class UralGun : MonoBehaviour {
	
	public Transform amm;
	public GameObject PosAmm;
	public GameObject point;
	public GameObject point1;
	public GameObject Fire1;
	public int speedAmm = 800;
	int i = 0;
	int j = 0;
	const float Z = 98.0f;
    const float X = 33.25628f;
	const float Y = 51.5f;
	const float Del = 6f;
	bool isFire = false;
	void Start() {
		
		//Fire1.enabled = false;
	}
	
	void Fire() {
			
			Transform g = (Transform) Instantiate(amm, transform.position, transform.rotation);
			PosAmm.transform.localPosition = new Vector3(X - i * Del, Y - j * Del, Z);
			//Fire1.GetComponent<ParticleSystem>().GetComponent<ParticleSystem>().looping = true;
			g.GetComponent<Rigidbody>().AddForce(transform.forward * speedAmm);
			point.GetComponent<Light>().enabled = true;
			point1.GetComponent<Light>().enabled = true;
			i++;
			if (i >= 9) {
				i = 0;
				j++;
			}
			if (j >= 4) {
				i = 0;
				j = 0;
			} 
	}
		

		


	void Update() {
		
		if (Input.GetKeyUp (KeyCode.R)) {
			Fire ();
		} else {
			point.GetComponent<Light>().enabled = false;
			point1.GetComponent<Light>().enabled = false;
			//Fire1.GetComponent<ParticleSystem>().GetComponent<ParticleSystem>().looping = false;
		}

		if (Input.GetKeyUp (KeyCode.T)) {
			isFire = true;
		}

		if (isFire) {
			FireWithPause ();
		}
	}

	private IEnumerator FireWithPause() {
		
		Fire ();
		yield return new WaitForSeconds(10.0f);

	}
    

}
