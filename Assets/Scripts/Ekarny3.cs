﻿using UnityEngine;
using System.Collections;

public class Ekarny3 : MonoBehaviour
{

    public AudioSource Ekarny;
    private bool Viclushen = true;
    // Use this for initialization
    void Start()
    {
        Ekarny = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R) && !Viclushen)
        {
            GetComponent<AudioSource>().Play();
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            Viclushen = !Viclushen;
        }
    }
}
