﻿using UnityEngine;
using System.Collections;

public class UralUpAbdDown : MonoBehaviour {

    public Transform Elements;
    public int speed;
    public int maxUp;

    private int num = 0;

    public KeyCode Up;
    public KeyCode Down;
  

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(Up))
        {
            if(Elements.localRotation.eulerAngles.x <= 10 || Elements.localRotation.eulerAngles.x >= 360 - maxUp)
                Elements.Rotate(new Vector3(-5, 0, 0), speed * Time.deltaTime);
        }
        else if (Input.GetKey(Down))
        {
            if (Elements.localRotation.x <= -0.01 || Elements.localRotation.x >= 0.01)
                Elements.Rotate(new Vector3(5, 0, 0), speed * Time.deltaTime);
            else
                Elements.Rotate(Vector3.right, -Elements.localEulerAngles.x);
        }
        
    }
}
