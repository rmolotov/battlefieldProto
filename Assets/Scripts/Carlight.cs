﻿using UnityEngine;
using System.Collections;

public class Carlight : MonoBehaviour {

    public Renderer[] breaklights;
    public Material breaklightON;
    public Material breaklightOFF;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.S))
        {
            for (int i = 0; i < 4; i++)
            {
                breaklights[i].material = breaklightON;
            }
        }
        else
        {
            for (int i = 0; i < 4; i++)
            {
                breaklights[i].material = breaklightOFF;
            }
        }
    }
}
