﻿using UnityEngine;
using System.Collections;

public class UralSoundEngine : MonoBehaviour {

    public Rigidbody rb;
	public Transform Vehicle;
    private AudioSource a;
    public float volume;
	public bool isEngine = false;
	
        // Use this for initialization
	void Start () {
            a = GetComponent<AudioSource>();
            rb.centerOfMass = new Vector3(0, 0, 0);
			//Vehicle.GetComponent<UralRearWheelDrive>().enabled = false;
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.V) && !isEngine)
        {
		
				GetComponent<AudioSource>().Play();
				isEngine = true;
			
		}
        a.volume = Mathf.Clamp(rb.velocity.magnitude / 14, 0.3f, 0.7f);
      
        if (Input.GetKeyDown(KeyCode.B) && isEngine)
        {
	
				GetComponent<AudioSource>().Stop();
				isEngine = false;
			
        }
		
		
    }
}
